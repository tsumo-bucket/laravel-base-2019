<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PageContentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page_id' => 'integer',

            'reference_id' => 'integer',

            'reference_type' => 'max:255',

            'name' => 'max:255',

            'slug' => 'max:255',

            'title' => 'max:255',

            'description' => '',

            'bg_image' => 'integer',

            'published' => 'in:draft,published',

            'order' => 'integer',

            'enabled' => '',

            'editable' => '',

            'allow_add_items' => '',
        ];
    }
}
